import datetime
import threading
import time

from sys import argv

import ps_drone
import requests

from camera import Camera

def startup():
    drone = ps_drone.Drone()
    drone.startup()
    drone.reset()

    while drone.getBattery()[0] == -1: time.sleep(0.1)
    drone.printGreen('Battery: ' + str(drone.getBattery()[0]) + '% ' + str(drone.getBattery()[1]))

    drone.useDemoMode(False)
    drone.getNDpackage(['altitude', 'demo', 'pressure_raw', 'wifi'])
    time.sleep(0.5)

    return drone

def collect_data(drone):
    NDC = drone.NavDataCount

    while True:
        while drone.NavDataCount == NDC: time.sleep(0.001)

        NDC = drone.NavDataCount

        data = (
            int(round(drone.NavData['altitude'][3] / 10)),
            int(round(drone.NavData['demo'][4][0] / 10)),
            int(round(drone.NavData['demo'][4][1] / 10)),
            int(round(drone.NavData['demo'][4][2] / 10)),
            drone.getBattery()[0],
            drone.NavData['wifi']
        )

        print '----------'
        print 'Altitude: ' + str(data[0])
        print 'Vx:       ' + str(data[1])
        print 'Vy:       ' + str(data[2])
        print 'Vz:       ' + str(data[3])
        print 'Batterie: ' + str(data[4]) + '% ' + str(drone.getBattery()[1])
        print 'WiFi:     ' + str(data[5])

        send_data(data)

def send_data(data):
    timestamp = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S:%f')

    URL = 'https://presalesbrbc871ee72.us1.hana.ondemand.com/201710_DRONE/table_insert.xsjs?'

    print '[%s] Sending message...' % (timestamp)
    try:
        r = requests.get(URL + 'timestamp={}&altitude={}&vx={}&vy={}&vz={}&battery={}&wifi={}'.format(timestamp.replace(' ', '%20'), *data))
    except requests.ConnectionError, requests.ConnectTimeout:
        print 'Cannot connect to the internet. Please, make sure you have internet connection. Trying to reconnect...\n'
        time.sleep(1)
    else:
        print '[%s] Message sent\n' % (timestamp)
        print '%d' % (r.status_code)

def main():
    drone = startup()

    t = threading.Thread(target=collect_data, args=(drone,))
    t.daemon = True
    t.start()

    if argv[1] == '-v':
        camera = Camera(drone)
        camera.showVideo()

    drone.setSpeed(0.75)

    drone.takeoff()
    time.sleep(5)

    drone.moveUp()
    time.sleep(2)

    drone.hover()
    time.sleep(2)

    drone.moveForward(1.0)
    time.sleep(2)

    drone.hover()
    time.sleep(2)

    drone.moveBackward(1.0)
    time.sleep(2)

    drone.hover()
    time.sleep(2)

    drone.turnLeft()
    time.sleep(2)

    drone.hover()
    time.sleep(2)

    drone.moveForward(1.0)
    time.sleep(2)

    drone.hover()
    time.sleep(2)

    drone.moveBackward(1.0)
    time.sleep(2)

    drone.hover()
    time.sleep(2)

    drone.turnRight()
    time.sleep(2)

    drone.hover()
    time.sleep(2)

    drone.doggyHop()
    time.sleep(3)

    drone.hover()
    time.sleep(3)

    drone.doggyNod()
    time.sleep(3)

    drone.hover()
    time.sleep(3)

    drone.doggyWag()
    time.sleep(3)

    drone.hover()
    time.sleep(3)

    drone.land()
    time.sleep(5)

    if argv[1] == '-v': camera.stopVideo()

    drone.printGreen('Battery: ' + str(drone.getBattery()[0]) + '% ' + str(drone.getBattery()[1]))

if __name__ == '__main__':
    main()
