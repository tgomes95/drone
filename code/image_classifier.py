import requests

def classify_image(image_file):
    URL = 'https://sandbox.api.sap.com/ml/imageclassifier/inference_sync'

    headers = {
    'Accept': 'application/json',
    'APIKey': 'N3V4fva1tuuJn158x8CzBWufAIA8A6X5'
    }

    files = {'files': open(image_file, 'rb')}

    j = None

    try:
        r = requests.post(URL, headers=headers, files=files)
    except requests.ConnectionError, requests.ConnectTimeout:
        print 'Cannot connect to the internet. Please, make sure you have internet connection. Trying to reconnect...\n'
        time.sleep(1)
    else:
        j = r.json()

    reset_classification()

    if j is not None and j['status'] == 'DONE':
        classification = j['predictions'][0]['results']
        print_classification(classification)
        send_classification(classification)

def print_classification(classification):
    for c in classification:
        print '%s: %.2f%%' % (c['label'].capitalize(), c['score'] * 100)

def send_classification(classification):
    for i, c in enumerate(classification, 1):
        update_classification(i, c['label'].capitalize(), c['score'] * 100)

    update_classification(6, 'UPDATED', 100)

def update_classification(id, label, score):
    URL = 'https://presalesbrbc871ee72.us1.hana.ondemand.com//201710_DRONE/update_classification_table.xsjs?id=%d&label=%s&score=%.2f'

    url = URL % (id, label, score)

    try:
        r = requests.get(url)
    except requests.ConnectionError, requests.ConnectTimeout:
        print 'Cannot connect to the internet. Please, make sure you have internet connection. Trying to reconnect...\n'
        time.sleep(1)

def reset_classification():
    URL = 'https://presalesbrbc871ee72.us1.hana.ondemand.com//201710_DRONE/reset_classification_table.xsjs'

    try:
        r = requests.get(URL)
    except requests.ConnectionError, requests.ConnectTimeout:
        print 'Cannot connect to the internet. Please, make sure you have internet connection. Trying to reconnect...\n'
        time.sleep(1)

def main():
    classify_image('../resources/dog.jpg')

if __name__ == '__main__':
    main()
