import datetime
import threading
import time

import ps_drone
import requests

from camera import Camera

from image_classifier import classify_image

def startup():
    drone = ps_drone.Drone()
    drone.startup()
    drone.reset()

    while drone.getBattery()[0] == -1: time.sleep(0.1)
    drone.printGreen('Battery: ' + str(drone.getBattery()[0]) + '% ' + str(drone.getBattery()[1]))

    drone.useDemoMode(False)
    drone.getNDpackage(['altitude', 'demo', 'pressure_raw', 'wifi'])
    time.sleep(0.5)

    return drone

def collect_data(drone):
    NDC = drone.NavDataCount

    while True:
        while drone.NavDataCount == NDC: time.sleep(0.001)

        NDC = drone.NavDataCount

        data = (
            int(round(drone.NavData['altitude'][3] / 10)),
            int(round(drone.NavData['demo'][4][0] / 10)),
            int(round(drone.NavData['demo'][4][1] / 10)),
            int(round(drone.NavData['demo'][4][2] / 10)),
            drone.getBattery()[0],
            drone.NavData['wifi']
        )

        print '----------'
        print 'Altitude: ' + str(data[0])
        print 'Vx:       ' + str(data[1])
        print 'Vy:       ' + str(data[2])
        print 'Vz:       ' + str(data[3])
        print 'Batterie: ' + str(data[4]) + '% ' + str(drone.getBattery()[1])
        print 'WiFi:     ' + str(data[5])

        send_data(data)

def send_data(data):
    timestamp = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S:%f')

    URL = 'https://presalesbrbc871ee72.us1.hana.ondemand.com/201710_DRONE/table_insert.xsjs?'

    print '[%s] Sending message...' % (timestamp)
    try:
        r = requests.get(URL + 'timestamp={}&altitude={}&vx={}&vy={}&vz={}&battery={}&wifi={}'.format(timestamp.replace(' ', '%20'), *data))
    except requests.ConnectionError, requests.ConnectTimeout:
        print 'Cannot connect to the internet. Please, make sure you have internet connection. Trying to reconnect...\n'
        time.sleep(1)
    else:
        print '[%s] Message sent\n' % (timestamp)
        print '%d' % (r.status_code)

def main():
    drone = startup()

    t = threading.Thread(target=collect_data, args=(drone,))
    t.daemon = True
    t.start()

    camera = None
    end = False

    while not end:
        key = drone.getKey()

        if key == ' ':
            if drone.NavData['demo'][0][2] and not drone.NavData['demo'][0][3]: drone.takeoff()
            else: drone.land()

        elif key == 'h': drone.hover()
        elif key == 'w': drone.moveForward()
        elif key == 's': drone.moveBackward()
        elif key == 'a': drone.moveLeft()
        elif key == 'd': drone.moveRight()
        elif key == 'q': drone.turnLeft()
        elif key == 'e': drone.turnRight()
        elif key == '7': drone.turnAngle(-10,1)
        elif key == '9': drone.turnAngle( 10,1)
        elif key == '4': drone.turnAngle(-45,1)
        elif key == '6': drone.turnAngle( 45,1)
        elif key == '1': drone.turnAngle(-90,1)
        elif key == '3': drone.turnAngle( 90,1)
        elif key == '8': drone.moveUp()
        elif key == '2': drone.moveDown()
        elif key == '*': drone.doggyHop()
        elif key == '+': drone.doggyNod()
        elif key == '-': drone.doggyWag()

        elif key == 'c' and camera is not None and camera.savedPicture: classify_image(camera.pictureFileName)

        elif key == 'p' and camera is not None: camera.pictureSwitch()

        elif key == 't':
            if camera is None: camera = Camera(drone)
            camera.takeAPicture()
            camera.showPicture()

        elif key == 'v':
            if camera is None: camera = Camera(drone)
            camera.videoSwitch()

        elif key != '': end = True

    drone.land()
    drone.printGreen('Battery: ' + str(drone.getBattery()[0]) + '% ' + str(drone.getBattery()[1]))

    if camera is not None: camera.stopVideo()

if __name__ == '__main__':
    main()
