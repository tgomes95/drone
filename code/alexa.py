import datetime
import threading
import time

import ps_drone
import requests

from camera import Camera

from image_classifier import classify_image

def startup():
    drone = ps_drone.Drone()
    drone.startup()
    drone.reset()

    while drone.getBattery()[0] == -1: time.sleep(0.1)
    drone.printGreen('Battery: ' + str(drone.getBattery()[0]) + '% ' + str(drone.getBattery()[1]))

    drone.useDemoMode(False)
    drone.getNDpackage(['altitude', 'demo', 'pressure_raw', 'wifi'])
    time.sleep(0.5)

    return drone

def collect_data(drone):
    NDC = drone.NavDataCount

    while True:
        while drone.NavDataCount == NDC: time.sleep(0.001)

        NDC = drone.NavDataCount

        data = (
            int(round(drone.NavData['altitude'][3] / 10)),
            int(round(drone.NavData['demo'][4][0] / 10)),
            int(round(drone.NavData['demo'][4][1] / 10)),
            int(round(drone.NavData['demo'][4][2] / 10)),
            drone.getBattery()[0],
            drone.NavData['wifi']
        )

        print '----------'
        print 'Altitude: ' + str(data[0])
        print 'Vx:       ' + str(data[1])
        print 'Vy:       ' + str(data[2])
        print 'Vz:       ' + str(data[3])
        print 'Batterie: ' + str(data[4]) + '% ' + str(drone.getBattery()[1])
        print 'WiFi:     ' + str(data[5])

        send_data(data)

def request_command():
    URL = 'https://presalesbrbc871ee72.us1.hana.ondemand.com//201710_DRONE/COMMAND.xsodata/COMMAND/?$format=json'

    j = None

    try:
        r = requests.get(URL)
    except requests.ConnectionError, requests.ConnectTimeout:
        print 'Cannot connect to the internet. Please, make sure you have internet connection. Trying to reconnect...\n'
        time.sleep(1)
    else:
        j = r.json()

    return j

def send_data(data):
    timestamp = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S:%f')

    URL = 'https://presalesbrbc871ee72.us1.hana.ondemand.com/201710_DRONE/table_insert.xsjs?'

    print '[%s] Sending message...' % (timestamp)
    try:
        r = requests.get(URL + 'timestamp={}&altitude={}&vx={}&vy={}&vz={}&battery={}&wifi={}'.format(timestamp.replace(' ', '%20'), *data))
    except requests.ConnectionError, requests.ConnectTimeout:
        print 'Cannot connect to the internet. Please, make sure you have internet connection. Trying to reconnect...\n'
        time.sleep(1)
    else:
        print '[%s] Message sent\n' % (timestamp)
        print '%d' % (r.status_code)

def update_command(command, duration=0):
    URL = 'https://presalesbrbc871ee72.us1.hana.ondemand.com//201710_DRONE/update_command_table.xsjs?command=%s&duration=%d' % (command, duration)

    while True:
        try:
            r = requests.get(URL)
        except requests.ConnectionError, requests.ConnectTimeout:
            print 'Cannot connect to the internet. Please, make sure you have internet connection. Trying to reconnect...\n'
            time.sleep(1)
        else:
            break

    return r.status_code

def main():
    drone = startup()

    t = threading.Thread(target=collect_data, args=(drone,))
    t.daemon = True
    t.start()

    update_command('land')
    previous_command = 'land'

    camera = None
    end = False

    while not end:
        c = request_command()

        if c is None:
            drone.land()
            time.sleep(1)
            continue

        command = c['d']['results'][0]['COMMAND']
        duration = c['d']['results'][0]['DURATION']

        if command == 'takeoff':
            print 'Take off'
            drone.takeoff()
            time.sleep(5)
            update_command('hover')

        elif command == 'land':
            print 'Land'
            drone.hover()
            drone.land()

        elif command == 'hover':
            print 'Hover'
            drone.hover()

        elif command == 'moveforward':
            print 'Move forward'
            drone.moveForward()

        elif command == 'moveforwardfor':
            print 'Move forward for %ds' % duration
            drone.moveForward()
            time.sleep(duration)
            drone.hover()
            update_command('hover')

        elif command == 'movebackward':
            print 'Move backward'
            drone.moveBackward()

        elif command == 'movebackwardfor':
            print 'Move backward for %ds' % duration
            drone.moveBackward()
            time.sleep(duration)
            drone.hover()
            update_command('hover')

        elif command == 'moveright':
            print 'Move right'
            drone.moveRight()

        elif command == 'moverightfor':
            print 'Move right for %ds' % duration
            drone.moveRight()
            time.sleep(duration)
            drone.hover()
            update_command('hover')

        elif command == 'moveleft':
            print 'Move left'
            drone.moveLeft()

        elif command == 'moveleftfor':
            print 'Move left for %ds' % duration
            drone.moveLeft()
            time.sleep(duration)
            drone.hover()
            update_command('hover')

        elif command == 'turnleft':
            print 'Turn left'
            drone.turnLeft()

        elif command == 'turnleftfor':
            print 'Turn left for %ds' % duration
            drone.turnLeft()
            time.sleep(duration)
            drone.hover()
            update_command('hover')

        elif command == 'turnright':
            print 'Turn right'
            drone.turnRight()

        elif command == 'turnrightfor':
            print 'Turn right for %ds' % duration
            drone.turnRight()
            time.sleep(duration)
            drone.hover()
            update_command('hover')

        elif command == 'moveup':
            print 'Move up'
            drone.moveUp()

        elif command == 'moveupfor':
            print 'Move up for %ds' % duration
            drone.moveUp()
            time.sleep(duration)
            drone.hover()
            update_command('hover')

        elif command == 'movedown':
            print 'Move down'
            drone.moveDown()

        elif command == 'movedownfor':
            print 'Move down for %ds' % duration
            drone.moveDown()
            time.sleep(duration)
            drone.hover()
            update_command('hover')

        elif command == 'showvideo':
            print 'Showing video'
            if camera is None: camera = Camera(drone)
            camera.showVideo()
            update_command(previous_command)

        elif command == 'hidevideo' and camera is not None:
            print 'Hiding video'
            camera.hideVideo()
            update_command(previous_command)

        elif command == 'takeapicture':
            print 'Taking a picture'
            if camera is None: camera = Camera(drone)
            camera.takeAPicture()
            camera.showPicture()
            update_command(previous_command)

        elif command == 'showpicture' and camera is not None:
            print 'Showing picture'
            camera.showPicture()
            update_command(previous_command)

        elif command == 'hidepicture' and camera is not None:
            print 'Hiding picture'
            camera.hidePicture()
            update_command(previous_command)

        elif command == 'classifyimage' and camera is not None and camera.savedPicture:
            print 'Classifying image'
            drone.hover()
            update_command('hover')
            classify_image(camera.pictureFileName)

        else:
            print 'Land'
            drone.hover()
            drone.land()
            update_command('land')
            end = True

        previous_command = command

    drone.land()
    drone.printGreen('Battery: ' + str(drone.getBattery()[0]) + '% ' + str(drone.getBattery()[1]))

    update_command('land')

    if camera is not None: camera.stopVideo()

if __name__ == '__main__':
    main()
