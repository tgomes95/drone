import time

import cv2

class Camera(object):

    def __init__(self, drone):
        self.__drone                = drone
        self.__videoHasStarted      = False
        self.__displayingVideo      = False
        self.__displayingPicture    = False
        self.__savedPicture         = False
        self.__picture              = None
        self.__pictureFileName      = '1.jpg'
        self.__cv2PictureWindowName = 'Picture'

        self.startVideo()

    def hidePicture(self):
        if self.__displayingPicture:
            cv2.destroyWindow(self.__cv2PictureWindowName)
            self.__displayingPicture = False

    def hideVideo(self):
        if self.__displayingVideo:
            self.__drone.hideVideo()
            self.__displayingVideo = False

    def pictureSwitch(self):
        if self.__displayingPicture: self.hidePicture()
        else:                        self.showPicture()

    def savePicture(self):
        if self.__picture is not None:
            cv2.imwrite(self.__pictureFileName, self.__picture)
            self.__savedPicture = True

    def showPicture(self):
        if self.__picture is not None:
            cv2.imshow(self.__cv2PictureWindowName, self.__picture)
            cv2.waitKey(1)
            self.__displayingPicture = True

    def showVideo(self):
        if not self.__displayingVideo:
            self.__drone.showVideo()
            self.__displayingVideo = True

    def startVideo(self):
        self.__drone.setConfigAllID()
        self.__drone.hdVideo()
        self.__drone.frontCam()
        self.__drone.startVideo()

        IMC = self.__drone.VideoImageCount
        while self.__drone.VideoImageCount == IMC: time.sleep(0.01)

        self.__videoHasStarted = True

    def stopVideo(self):
        if self.__displayingPicture: self.hidePicture()
        if self.__displayingVideo:   self.__drone.hideVideo()
        if self.__videoHasStarted:   self.__drone.stopVideo()

    def takeAPicture(self):
        self.__picture = self.__drone.VideoImage
        self.savePicture()

    def videoSwitch(self):
        if self.__displayingVideo: self.hideVideo()
        else:                      self.showVideo()

    @property
    def picture(self):
        return self.__picture

    @property
    def pictureFileName(self):
        return self.__pictureFileName

    @property
    def savedPicture(self):
        return self.__savedPicture
