from __future__ import print_function

import json
import os
import time
import urllib2


# ----------------------------------- Global -----------------------------------


COMMANDS = {
'Takeoff':       ['take off', 'takeoff', 'Taking off'],
'Land':          ['land', 'land', 'Landing'],
'Hover':         ['hover', 'hover', 'Hovering'],
'MoveForward':   ['move', 'moveforward', 'Moving forward'],
'MoveBackward':  ['move', 'movebackward', 'Moving backward'],
'MoveLeft':      ['move', 'moveleft', 'Moving left'],
'MoveRight':     ['move', 'moveright', 'Moving right'],
'TurnLeft':      ['turn', 'turnleft', 'Turning left'],
'TurnRight':     ['turn', 'turnright', 'Turning right'],
'MoveUp':        ['move', 'moveup', 'Moving up'],
'MoveDown':      ['move', 'movedown', 'Moving down'],
'ShowVideo':     ['show video', 'showvideo', 'Showing video'],
'HideVideo':     ['hide video', 'hidevideo', 'Hiding video'],
'TakeAPicture':  ['take a picture', 'takeapicture', 'Taking a picture'],
'ShowPicture':   ['show picture', 'showpicture', 'Showing picture'],
'HidePicture':   ['hide picture', 'hidepicture', 'Hiding picture'],
'ClassifyImage': ['classify image', 'classifyimage', 'Classifying image'],
'End':           ['end', 'end', 'Ending']
}


COMMANDS_WITH_DURATION = {
'MoveForwardFor':  ['move', 'moveforwardfor', 'Moving forward'],
'MoveBackwardFor': ['move', 'movebackwardfor', 'Moving backward'],
'MoveLeftFor':     ['move', 'moveleftfor', 'Moving left'],
'MoveRightFor':    ['move', 'moverightfor', 'Moving right'],
'TurnLeftFor':     ['turn', 'turnleftfor', 'Turning left'],
'TurnRightFor':    ['turn', 'turnrightfor', 'Turning right'],
'MoveUpFor':       ['move', 'moveupfor', 'Moving up'],
'MoveDownFor':     ['move', 'movedownfor', 'Moving down']
}


# --------------- Helpers that build all of the responses ----------------------


def build_speechlet_response(title, output, reprompt_text, should_end_session):
    return {
        'outputSpeech': {
            'type': 'PlainText',
            'text': output
        },
        'card': {
            'type': 'Simple',
            'title': "SessionSpeechlet - " + title,
            'content': "SessionSpeechlet - " + output
        },
        'reprompt': {
            'outputSpeech': {
                'type': 'PlainText',
                'text': reprompt_text
            }
        },
        'shouldEndSession': should_end_session
    }


def build_response(session_attributes, speechlet_response):
    return {
        'version': '1.0',
        'sessionAttributes': session_attributes,
        'response': speechlet_response
    }


# --------------- Functions that control the skill's behavior ------------------


def get_welcome_response():
    """ If we wanted to initialize the session to have some attributes we could
    add those here
    """

    session_attributes = {}

    card_title = "Welcome"

    speech_output = "Welcome to the Drone Skill. " \
                    "Ready for a command... "

    # If the user either does not reply to the welcome message or says something
    # that is not understood, they will be prompted again with this text.
    reprompt_text = "Please, give the drone a command. "

    should_end_session = False

    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def handle_session_end_request():
    card_title = "Session Ended"

    speech_output = "Thank you for trying the Drone Skill. " \
                    "Have a nice day! "

    # Setting this to true ends the session and exits the skill.
    should_end_session = True

    return build_response({}, build_speechlet_response(
        card_title, speech_output, None, should_end_session))


def update_command(url):
    URL = os.environ['COMMAND_URL']

    request = urllib2.Request(URL + url)

    r = urllib2.urlopen(request)
    j = json.load(r)

    return j


def request_classification():
    URL = os.environ['CLASSIFICATION_URL']

    request = urllib2.Request(URL)

    r = urllib2.urlopen(request)
    j = json.load(r)

    return j


def execute_command(intent, session, act, command, text):
    session_attributes = {}
    reprompt_text = 'Waiting for the next command...'
    should_end_session = False if command != 'end' else True

    url = 'command=%s&duration=0' % command

    j = update_command(url)

    speech_output = text

    return build_response(session_attributes, build_speechlet_response(
        intent['name'], speech_output, reprompt_text, should_end_session))


def execute_command_with_duration(intent, session, act, command, text):
    session_attributes = {}
    reprompt_text = 'Waiting for the next command...'
    should_end_session = False

    try:
        duration = int(intent['slots']['duration']['value'])
    except ValueError:
        duration = 0

    if 1 <= duration <= 10:
        url = 'command=%s&duration=%d' % (command, duration)
    else:
        url = 'command=hover&duration=0'

    j = update_command(url)

    if duration == 1:
        speech_output = '%s for one second' % text
    elif 2 <= duration <= 10:
        speech_output = '%s for %d seconds' % (text, duration)
    elif duration > 10:
        speech_output = 'Sorry. It is too dangerous to %s for such a long time. Please, try again' % act
    else:
        speech_output = 'Sorry. I cannot %s for that amount of time or I could not understand. Please, try again' % act

    return build_response(session_attributes, build_speechlet_response(
        intent['name'], speech_output, reprompt_text, should_end_session))


def read_classification(intent, session):
    session_attributes = {}
    reprompt_text = 'Waiting for the next command...'
    should_end_session = False

    time.sleep(5)

    j = request_classification()
    r = j['d']['results']

    if (r[5]['LABEL'], r[5]['SCORE']) == ('UPDATED', '100'):
        speech_output = 'The classification is: ' + '. '.join('%s with a %s%% chance' % (i['LABEL'], i['SCORE']) for i in r[:-1])
    else:
        speech_output = 'The classification is not ready yet. Please, try again in a few moments'

    return build_response(session_attributes, build_speechlet_response(
        intent['name'], speech_output, reprompt_text, should_end_session))


def wait(intent, session):
    session_attributes = {}
    reprompt_text = 'Waiting for the next command...'
    should_end_session = False

    time.sleep(5)

    speech_output = 'Waiting'

    return build_response(session_attributes, build_speechlet_response(
        intent['name'], speech_output, reprompt_text, should_end_session))


# --------------- Events ------------------


def on_session_started(session_started_request, session):
    """ Called when the session starts """

    print("on_session_started requestId=" + session_started_request['requestId']
          + ", sessionId=" + session['sessionId'])


def on_launch(launch_request, session):
    """ Called when the user launches the skill without specifying what they
    want
    """

    print("on_launch requestId=" + launch_request['requestId'] +
          ", sessionId=" + session['sessionId'])

    # Dispatch to your skill's launch
    return get_welcome_response()


def on_intent(intent_request, session):
    """ Called when the user specifies an intent for this skill """

    print("on_intent requestId=" + intent_request['requestId'] +
          ", sessionId=" + session['sessionId'])

    intent = intent_request['intent']
    intent_name = intent_request['intent']['name']

    # Dispatch to your skill's intent handlers
    if intent_name in COMMANDS:
        return execute_command(intent, session, *COMMANDS[intent_name])

    elif intent_name in COMMANDS_WITH_DURATION:
        return execute_command_with_duration(intent, session, *COMMANDS_WITH_DURATION[intent_name])

    elif intent_name == 'ReadClassification':
        return read_classification(intent, session)

    elif intent_name == 'Wait':
        return wait(intent, session)

    elif intent_name == "AMAZON.HelpIntent":
        return get_welcome_response()

    elif intent_name == "AMAZON.CancelIntent" or intent_name == "AMAZON.StopIntent":
        return handle_session_end_request()

    else:
        raise ValueError("Invalid intent")


def on_session_ended(session_ended_request, session):
    """ Called when the user ends the session.

    Is not called when the skill returns should_end_session=true
    """

    print("on_session_ended requestId=" + session_ended_request['requestId'] +
          ", sessionId=" + session['sessionId'])


# --------------- Main handler ------------------


def lambda_handler(event, context):
    """ Route the incoming request based on type (LaunchRequest, IntentRequest,
    etc.) The JSON body of the request is provided in the event parameter.
    """

    print("event.session.application.applicationId=" +
          event['session']['application']['applicationId'])

    """
    Uncomment this if statement and populate with your skill's application ID to
    prevent someone else from configuring a skill that sends requests to this
    function.
    """
    # if (event['session']['application']['applicationId'] !=
    #         "amzn1.echo-sdk-ams.app.[unique-value-here]"):
    #     raise ValueError("Invalid Application ID")

    if event['session']['new']:
        on_session_started({'requestId': event['request']['requestId']},
                           event['session'])

    if event['request']['type'] == "LaunchRequest":
        return on_launch(event['request'], event['session'])
    elif event['request']['type'] == "IntentRequest":
        return on_intent(event['request'], event['session'])
    elif event['request']['type'] == "SessionEndedRequest":
        return on_session_ended(event['request'], event['session'])
