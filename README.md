# Drone

Based on Parrot AR.Drone 2.0

## Instructions

### Install python and pip

```
#!bash

# Install python and pip
sudo apt-get install -y python python-pip
```

### Install dependencies

```
#!bash

# Install python dependencies
sudo pip install -U requests opencv-python
```

### Connect the drone to the computer

+ Insert an WiFi adapter to the computer
+ Connect the WiFi adapter to the drone

### Run

There are three running modes

### 1. Keyboard

Key        | Command
---------- | ---------------
Spacebar   | Take off / Land
H          | Hover
W          | Move forward
S          | Move backward
A          | Move left
D          | Move right
Q          | Turn left
E          | Turn right
4          | Turn angle -45º
6          | Turn angle  45º
1          | Turn angle -90º
3          | Turn angle  90º
8          | Move up
2          | Move down
T          | Take a picture
P          | Show / Hide picture
V          | Show / Hide video
*          | Doggy Hop
+          | Doggy Nod
-          | Doggy Wag
Return Key | End

```
#!bash

cd code/
python keyboard.py
```

### 2. Autopilot

Pre-established route in the code

1. Take off
2. Move up for 2 seconds
3. Move forward for 2 seconds with full speed
4. Move backward for 2 seconds with full speed
5. Turn left for 2 seconds
6. Move forward for 2 seconds with full speed
7. Move backward for 2 seconds with full speed
8. Turn right for 2 seconds
9. Doggy hop
10. Doggy nod
11. Doggy wag
12. Land

```
#!bash

cd code/
python autopilot.py
```

### 3. Voice

Voice mode with Amazon's Alexa

Setup an Alexa Skill using the files

+ _skill/interaction_model.json_
+ _skill/lambda.py_

Available voice commands

+ Take off
+ Move _forward / left / right / backward_ ( for _1..10_ seconds )
+ Turn _left / right_ ( for _1..10_ seconds )
+ Show / Hide video
+ Take a picture
+ Show / Hide picture
+ Classify picture
+ Read classification
+ Land

```
#!bash

cd code/
python alexa.py
```

## API

[PS-Drone](http://www.playsheep.de/drone/downloads.html)
