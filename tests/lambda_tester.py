import json
import time
import urllib2


COMMANDS = {
'Takeoff':       ['take off', 'takeoff', 'Taking off'],
'Land':          ['land', 'land', 'Landing'],
'Hover':         ['hover', 'hover', 'Hovering'],
'MoveForward':   ['move', 'moveforward', 'Moving forward'],
'MoveBackward':  ['move', 'movebackward', 'Moving backward'],
'MoveLeft':      ['move', 'moveleft', 'Moving left'],
'MoveRight':     ['move', 'moveright', 'Moving right'],
'TurnLeft':      ['turn', 'turnleft', 'Turning left'],
'TurnRight':     ['turn', 'turnright', 'Turning right'],
'MoveUp':        ['move', 'moveup', 'Moving up'],
'MoveDown':      ['move', 'movedown', 'Moving down'],
'ShowVideo':     ['show video', 'showvideo', 'Showing video'],
'HideVideo':     ['hide video', 'hidevideo', 'Hiding video'],
'TakeAPicture':  ['take a picture', 'takeapicture', 'Taking a picture'],
'ShowPicture':   ['show picture', 'showpicture', 'Showing picture'],
'HidePicture':   ['hide picture', 'hidepicture', 'Hiding picture'],
'ClassifyImage': ['classify image', 'classifyimage', 'Classifying image'],
}


COMMANDS_WITH_DURATION = {
'MoveForwardFor':  ['move', 'moveforwardfor', 'Moving forward'],
'MoveBackwardFor': ['move', 'movebackwardfor', 'Moving backward'],
'MoveLeftFor':     ['move', 'moveleftfor', 'Moving left'],
'MoveRightFor':    ['move', 'moverightfor', 'Moving right'],
'TurnLeftFor':     ['turn', 'turnleftfor', 'Turning left'],
'TurnRightFor':    ['turn', 'turnrightfor', 'Turning right'],
'MoveUpFor':       ['move', 'moveupfor', 'Moving up'],
'MoveDownFor':     ['move', 'movedownfor', 'Moving down']
}


SERVICES = {
'ClassifyImage': ['classify image', 'classifyimage', 'Classifying image']
}


def update_command(url):
    URL = 'https://presalesbrbc871ee72.us1.hana.ondemand.com//201710_DRONE/update_command_table.xsjs?'

    request = urllib2.Request(URL + url)

    r = urllib2.urlopen(request)
    j = json.load(r)

    return j


def request_classification():
    URL = 'https://presalesbrbc871ee72.us1.hana.ondemand.com//201710_DRONE/CLASSIFICATION.xsodata/CLASSIFICATION/?$format=json'

    request = urllib2.Request(URL)

    r = urllib2.urlopen(request)
    j = json.load(r)

    return j


def execute_command(act, command, text):

    url = 'command=%s&duration=0' % command

    j = update_command(url)

    speech_output = text

    print speech_output


def execute_command_with_duration(act, command, text):
    try:
        duration = 1
    except ValueError:
        duration = 0

    if 1 <= duration <= 10:
        url = 'command=%s&duration=%d' % (command, duration)
    else:
        url = 'command=hover&duration=0'

    j = update_command(url)

    if duration == 1:
        speech_output = '%s for one second' % text
    elif 2 <= duration <= 10:
        speech_output = '%s for %d seconds' % (text, duration)
    elif duration > 10:
        speech_output = 'Sorry. It is too dangerous to %s for such a long time. Please, try again' % act
    else:
        speech_output = 'Sorry. I cannot %s for that amount of time or I could not understand. Please, try again' % act

    print speech_output


def read_classification():
    time.sleep(5)

    j = request_classification()
    r = j['d']['results']

    if (r[5]['LABEL'], r[5]['SCORE']) == ('UPDATED', '100'):
        speech_output = 'The classification is: ' + '. '.join('%s with a %s%% chance' % (i['LABEL'], i['SCORE']) for i in r[:-1])
    else:
        speech_output = 'The classification is not ready yet. Please, try again in a few moments'

    print speech_output


def wait():
    speech_output = 'Waiting'

    time.sleep(5)

    print speech_output


def classify_image():
    execute_command('classify image', 'classifyimage', 'Classifying image')


def end():
    execute_command('end', 'end', 'Ending')


def main():
    for c in COMMANDS:
        execute_command(*COMMANDS[c])
        time.sleep(8)

    for c in COMMANDS_WITH_DURATION:
        execute_command_with_duration(*COMMANDS_WITH_DURATION[c])
        time.sleep(8)

    classify_image()
    read_classification()
    end()

if __name__ == '__main__':
    main()
