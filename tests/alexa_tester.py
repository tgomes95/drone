import time

import requests

from image_classifier import classify_image

def request_command():
    URL = 'https://presalesbrbc871ee72.us1.hana.ondemand.com//201710_DRONE/COMMAND.xsodata/COMMAND/?$format=json'

    j = None

    try:
        r = requests.get(URL)
    except requests.ConnectionError, requests.ConnectTimeout:
        print 'Cannot connect to the internet. Please, make sure you have internet connection. Trying to reconnect...\n'
        time.sleep(1)
    else:
        j = r.json()

    return j

def update_command(command, duration=0):
    URL = 'https://presalesbrbc871ee72.us1.hana.ondemand.com//201710_DRONE/update_command_table.xsjs?command=%s&duration=%d' % (command, duration)

    while True:
        try:
            r = requests.get(URL)
        except requests.ConnectionError, requests.ConnectTimeout:
            print 'Cannot connect to the internet. Please, make sure you have internet connection. Trying to reconnect...\n'
            time.sleep(1)
        else:
            break

    return r.status_code

def main():
    update_command('land')
    previous_command = 'land'

    end = False

    while not end:
        c = request_command()

        if c is None:
            time.sleep(1)
            continue

        command = c['d']['results'][0]['COMMAND']
        duration = c['d']['results'][0]['DURATION']

        if command == 'takeoff':
            print 'Take off'
            time.sleep(5)
            update_command('hover')

        elif command == 'land':
            print 'Land'

        elif command == 'hover':
            print 'Hover'

        elif command == 'moveforward':
            print 'Move forward'

        elif command == 'moveforwardfor':
            print 'Move forward for %ds' % duration
            time.sleep(duration)
            update_command('hover')

        elif command == 'movebackward':
            print 'Move backward'

        elif command == 'movebackwardfor':
            print 'Move backward for %ds' % duration
            time.sleep(duration)
            update_command('hover')

        elif command == 'moveright':
            print 'Move right'

        elif command == 'moverightfor':
            print 'Move right for %ds' % duration
            time.sleep(duration)
            update_command('hover')

        elif command == 'moveleft':
            print 'Move left'

        elif command == 'moveleftfor':
            print 'Move left for %ds' % duration
            time.sleep(duration)
            update_command('hover')

        elif command == 'turnleft':
            print 'Turn left'

        elif command == 'turnleftfor':
            print 'Turn left for %ds' % duration
            time.sleep(duration)
            update_command('hover')

        elif command == 'turnright':
            print 'Turn right'

        elif command == 'turnrightfor':
            print 'Turn right for %ds' % duration
            time.sleep(duration)
            update_command('hover')

        elif command == 'moveup':
            print 'Move up'

        elif command == 'moveupfor':
            print 'Move up for %ds' % duration
            time.sleep(duration)
            update_command('hover')

        elif command == 'movedown':
            print 'Move down'

        elif command == 'movedownfor':
            print 'Move down for %ds' % duration
            time.sleep(duration)
            update_command('hover')

        elif command == 'showvideo':
            print 'Showing video'
            update_command(previous_command)

        elif command == 'hidevideo':
            print 'Hiding video'
            update_command(previous_command)

        elif command == 'takeapicture':
            print 'Taking a picture'
            update_command(previous_command)

        elif command == 'showpicture':
            print 'Showing picture'
            update_command(previous_command)

        elif command == 'hidepicture':
            print 'Hiding picture'
            update_command(previous_command)

        elif command == 'classifyimage':
            print 'Classifying image'
            update_command('hover')
            classify_image('../resources/dog.jpg')

        else:
            print 'Land'
            update_command('land')
            end = True

        previous_command = command

    update_command('land')

if __name__ == '__main__':
    main()
