from threading import Thread
from time import sleep

def func1(i):
    print 'Started', i
    sleep(i)
    print 'Function', i

if __name__ == '__main__':
    for i in xrange(1,3):
        t = Thread(target = func1, args = (i,))
        t.start()
