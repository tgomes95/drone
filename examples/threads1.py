from threading import Thread

def func1():
    print 'Function 1'

def func2():
    print 'Function 2'

if __name__ == '__main__':
    Thread(target = func1).start()
    Thread(target = func2).start()
